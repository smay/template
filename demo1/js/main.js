$(document).ready(function() {


     (function(){
          function setup() {
               $('.run:eq(0)').animate({
                    left: '0'
               }, 1300)
               $('.run:eq(1)').animate({
                    left: '0'
               }, 1800)
               $('.run:eq(2)').animate({
                    left: '0'
               }, 2000)
          }
          $('.content').animate({top: 0}, 1000, setup)
          
     })()

     function a1() {
          $('#btn').on('mouseenter', function() {
               $(this).off('mouseenter')
               $('.hover').animate({left: 250}, 900, function() {
                    $(this).css('left', 0)
                    a1()
               })
          })
     }

     a1()
})