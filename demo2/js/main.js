(function () {
     $(window).on('scroll', function () {
          if ($(window).scrollTop()) {
               $('.navigation').addClass('black')
          } else {
               $('.navigation').removeClass('black')
          }
     })

     $(window).resize(function () {
          if ($(window).width() <= 720) {
               $('ul').hide()
               $('.menu-icon').addClass('click')
          } else {
               $('ul').show()
               $('.menu-icon').removeClass('click')
          }
     })
})()

$(document).ready(function () {
     if ($(window).width() <= 720) {
          $('.menu-icon').addClass('click')
     }
     $('.logo').on('click', '.click', function () {
          $('ul').slideToggle()
     })
})